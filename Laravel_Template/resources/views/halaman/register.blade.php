<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<form action="kirim" method="POST">
		@csrf
		<label> First name: </label><br><br>
		<input type="text" name="fname"><br><br>
		<label> Last name: </label><br><br>
		<input type="text" name="lname"><br><br>
		<label> Gender: </label><br><br>
		<input type="radio" name="gender">Male<br>
		<input type="radio" name="gender">Female<br>
		<input type="radio" name="gender">Other<br><br>
		<label> Nationality: </label><br><br>
		<select name="wn">
			<option value="indonesia">Indonesian</option>
			<option value="asing">US</option>
		</select><br><br>
		<label> Language Spoken: </label><br><br>
		<input type="checkbox">Bahasa Indonesia<br>
		<input type="checkbox">English<br>
		<input type="checkbox">Other<br><br>
		<label>Bio</label><br>
		<textarea name="bio" id="" cols="30" rows="10"></textarea><br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>